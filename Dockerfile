################################################################
#     BUILD
################################################################

FROM timbru31/java-node:latest as build
ARG version=0.0.0-docker
WORKDIR /app
COPY . .
RUN npm ci
RUN npm version $version --no-git-tag-version
RUN npm run build -- --prod


################################################################
#     DEPLOY
################################################################

FROM nginx:1.19.2-alpine as deploy

RUN apk --no-cache add bash=~5.0

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx
COPY --from=build /app/dist/f1-league-web ./html
