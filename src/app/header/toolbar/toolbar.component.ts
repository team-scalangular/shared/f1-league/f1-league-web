import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {UiService} from '../../shared/services/ui.service';
import {TokenService} from '../../shared/services/token.service';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  @Input() isDarkThemed$: Observable<boolean>;
  @Output() toggleSidenavRequest = new EventEmitter<void>();
  tokenIsSet: boolean;
  subscriptions: Subscription;
  isLeagueView = false;

  constructor(
    private uiService: UiService,
    private tokenService: TokenService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        this.isLeagueView = this.router.url === '/';
      });
    this.subscriptions = this.tokenService.tokenIsSet.subscribe(tokenIsSet => {
      this.tokenIsSet = tokenIsSet;
    });
  }

  dispatchToggleSidenavRequest(): void {
    this.toggleSidenavRequest.emit();
  }

  openTokenDialog(): void {
    this.uiService.openTokenDialog();
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  clearToken(): void {
    this.tokenService.clearToken();
  }
}
