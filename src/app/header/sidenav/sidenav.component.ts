import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {TokenService} from '../../shared/services/token.service';
import {UiService} from '../../shared/services/ui.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit, OnDestroy {
  isLeagueView: boolean;
  tokenIsSet: boolean;
  subscriptions: Subscription;


  constructor(
    private tokenService: TokenService,
    private uiService: UiService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        this.isLeagueView = this.router.url === '/';
      });
    this.subscriptions = this.tokenService.tokenIsSet.subscribe(tokenIsSet => {
      this.tokenIsSet = tokenIsSet;
    });
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  clearToken(): void {
    this.tokenService.clearToken();
  }

  openTokenDialog(): void {
    this.uiService.openTokenDialog();
  }

}
