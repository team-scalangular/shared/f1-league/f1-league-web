import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromUI from './shared/store/ui/ui.reducer';
import * as fromLeagues from './shared/store/leagues/leagues.reducer';
import {getError, LoadingState} from './shared/store/interfaces/call-state';

export interface State {
  ui: fromUI.State;
  leagues: fromLeagues.State;
}

export const reducers: ActionReducerMap<State> = {
  ui: fromUI.uiReducer,
  leagues: fromLeagues.leaguesReducer
};

export const getUiState = createFeatureSelector<fromUI.State>('ui');
export const getIsLoading = createSelector(getUiState, fromUI.getIsLoading);
export const getIsDarkThemed = createSelector(getUiState, fromUI.getIsDarkThemed);

export const getLeaguesState = createFeatureSelector<fromLeagues.State>('leagues');
export const getLeagues = createSelector(getLeaguesState, fromLeagues.getLeagues);
export const getSelectedLeague = createSelector(getLeaguesState, fromLeagues.getSelectedLeague);
export const getIsLoadingLeagues = createSelector(getLeaguesState, state => state.callState === LoadingState.LOADING);
export const getErrorLeagues = createSelector(getLeaguesState, state => getError(state.callState));
