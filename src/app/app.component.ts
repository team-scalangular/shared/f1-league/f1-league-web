import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromRoot from './app.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'f1-league-web';
  isDarkThemed$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) {
  }

  ngOnInit(): void {
    this.isDarkThemed$ = this.store.select(fromRoot.getIsDarkThemed);
  }
}
