import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  Car,
  Driver,
  DriverClassificationService,
  DriverRaceHistoryService,
  League,
  Race,
  RaceService,
  Team
} from '../../../../build/openapi';
import {MatTableDataSource} from '@angular/material/table';
import {LeagueViewService} from '../league-view/services/league-view.service';
import {Subscription} from 'rxjs';

export interface DriverRaceResult {
  raceIndex: number;
  circuit: string;
  endPosition: number;
  points: string;
  totalPoints: number;
  totalPosition: number;
  hasFastestLap: boolean;
  car: Car;
}

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss']
})
export class DriverComponent implements OnInit, OnDestroy {
  loadedTable = false;
  loadingFinished = false;
  noContent = false;

  @Input() driverToShow: Driver;
  @Input() selectedLeague: League;
  races: Race[] = [];
  mapDriverToTeam: Map<string, Team> = new Map<string, Team>();

  columnsToDisplay = ['circuit', 'endPosition', 'points', 'totalPoints', 'totalPosition', 'car'];
  dataSource: MatTableDataSource<DriverRaceResult> = new MatTableDataSource<DriverRaceResult>();

  getRacesSub: Subscription;

  constructor(
    private driverClassificationService: DriverClassificationService,
    private raceService: RaceService,
    private driverRaceHistoryService: DriverRaceHistoryService,
    private leagueViewService: LeagueViewService
  ) {
  }

  ngOnInit(): void {
    this.getRacesSub = this.raceService.getRaces(this.selectedLeague.leagueId)
      .subscribe(races => {
        this.races = races;
        this.loadingFinished = true;
      });
    this.mapDriverToTeam = this.leagueViewService.mapDriverToTeam;
  }

  loadRaceResults(): void {
    if (this.loadedTable) {
      return;
    }

    this.races = this.races.sort((a, b) => b.racePositionIndex - a.racePositionIndex);

    this.races.forEach(race => {
      this.driverRaceHistoryService.getDriverRaceHistoryByDriverId(this.driverToShow.driverId, race.raceId).subscribe(raceHistory => {
        if (raceHistory == null) {
          this.loadedTable = true;
          return;
        }

        const pointsForRace: string = raceHistory.hasFastestLap ? (raceHistory.pointsForRace + 1)
          .toString() : raceHistory.pointsForRace.toString();

        const aRaceResult: DriverRaceResult = {
          raceIndex: race.racePositionIndex,
          circuit: (race.racePositionIndex + 1).toString() + ' ' + race.circuit,
          endPosition: raceHistory.endpostion,
          points: pointsForRace,
          totalPoints: raceHistory.totalPointsAtRaceStanding,
          totalPosition: raceHistory.classificationStandingAtRace,
          hasFastestLap: raceHistory.hasFastestLap,
          car: raceHistory.carInRace
        };
        const data = this.dataSource.data;
        data.push(aRaceResult);
        data.sort((a, b) => a.raceIndex - b.raceIndex);
        this.dataSource.data = data;
        this.loadedTable = true;
        this.noContent = this.dataSource.data.length == 0 && this.loadedTable == true;
      }, error => {
      }, () => this.noContent = this.dataSource.data.length === 0);
    });
  }

  ngOnDestroy(): void {
    if (this.getRacesSub) {
      this.getRacesSub.unsubscribe();
    }
  }
}
