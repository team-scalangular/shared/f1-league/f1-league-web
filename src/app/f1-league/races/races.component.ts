import {Component, OnInit} from '@angular/core';
import {League, Race, RaceService} from '../../../../build/openapi';
import {LeagueViewService} from '../league-view/services/league-view.service';
import {filter, flatMap} from 'rxjs/operators';

@Component({
  selector: 'app-races',
  templateUrl: './races.component.html',
  styleUrls: ['./races.component.scss']
})
export class RacesComponent implements OnInit {
  selectedLeague: League;
  races: Race[] = [];

  constructor(private raceService: RaceService,
              private leagueViewService: LeagueViewService) {
  }

  ngOnInit(): void {
    this.leagueViewService.selectedLeagueChanges
      .pipe(
        filter(league => league !== undefined),
        flatMap(league => {
          this.selectedLeague = league;
          return this.raceService.getRaces(this.selectedLeague.leagueId);
        })
      ).pipe(filter(races => !!races))
      .subscribe(races => {
        this.races = races.sort((a, b) => a.racePositionIndex - b.racePositionIndex);
      });
  }
}
