import {Component, Input, OnInit} from '@angular/core';
import {Car, DriverRaceResult, Race, RaceResultsService} from '../../../../../build/openapi';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {TokenService} from '../../../shared/services/token.service';

export interface DriverRaceResultRow {
  endPosition: number;
  driver: string;
  grid: number;
  points: string;
  fastestLap: number;
  hasFastestLap: boolean;
  car: Car;
}

@Component({
  selector: 'app-race-view',
  templateUrl: './race-view.component.html',
  styleUrls: ['./race-view.component.scss']
})
export class RaceViewComponent implements OnInit {
  loaded = false;
  isEditor: boolean;

  @Input() race: Race;
  driverRaceResults: DriverRaceResult[] = [];

  columnsToDisplay = ['endPosition', 'driver', 'grid', 'points', 'fastestLap', 'car'];
  dataSource: MatTableDataSource<DriverRaceResultRow> = new MatTableDataSource<DriverRaceResultRow>();

  constructor(
    private raceResultsService: RaceResultsService,
    private router: Router,
    private tokenService: TokenService
  ) {
  }

  ngOnInit(): void {
    this.tokenService.tokenIsSet
      .subscribe(isSet => {
        this.isEditor = isSet;
      });
  }

  loadResults() {
    if (this.loaded) {
      return;
    }

    this.raceResultsService.getRaceResultsByRaceId(this.race.raceId).subscribe(driverRaceResults => {
      this.driverRaceResults = driverRaceResults.sort((a, b) => b.endpostion - a.endpostion);
      const data = this.dataSource.data;
      this.driverRaceResults.forEach(driverRaceResult => {
        const aDriverRaceResult: DriverRaceResultRow = {
          endPosition: driverRaceResult.endpostion,
          driver: driverRaceResult.driverName,
          grid: driverRaceResult.startposition,
          points: driverRaceResult.points.toString(),
          fastestLap: driverRaceResult.fastestLap,
          hasFastestLap: driverRaceResult.hasFastestLap,
          car: driverRaceResult.carInRace
        };
        data.push(aDriverRaceResult);
      });

      data.sort((a, b) => a.endPosition - b.endPosition);
      this.dataSource.data = data;
      this.loaded = true;
    });
  }

  forwardToAddRaceResult() {
    this.router.navigate(['league/edit/raceResults'], {queryParams: {raceId: this.race.raceId}});
  }
}
