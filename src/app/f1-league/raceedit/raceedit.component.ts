import {Component, OnInit} from '@angular/core';
import {filter, flatMap} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {DriverRaceResult, Race} from '../../../../build/openapi';
import {LeagueViewService} from '../league-view/services/league-view.service';
import {RaceEditService} from './race-edit.service';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-raceedit',
  templateUrl: './raceedit.component.html',
  styleUrls: ['./raceedit.component.scss']
})
export class RaceeditComponent implements OnInit {

  raceId: string;
  race: Race;
  subscriptions: Subscription = new Subscription();
  columnsToDisplay = ['endPosition', 'driver', 'grid', 'fastestLap', 'car'];
  dataSource: MatTableDataSource<DriverRaceResult> = new MatTableDataSource<DriverRaceResult>();

  constructor(private route: ActivatedRoute,
              public raceEditService: RaceEditService,
              public leagueViewService: LeagueViewService) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.route.queryParams
      .pipe(
        filter(params => params.raceId),
        flatMap(params => {
          this.raceId = params.raceId;
          return this.leagueViewService.selectedLeagueChanges;
        })
      )
      .subscribe((league) => {
        this.raceEditService.loadRaceToEdit(league.leagueId, this.raceId);
      })
    );
    this.loadNewestResultsToAdd();
  }

  loadNewestResultsToAdd(): void {
    let data: DriverRaceResult[];
    data = this.raceEditService.raceResultsToPost;
    this.dataSource.data = data;
  }
}
