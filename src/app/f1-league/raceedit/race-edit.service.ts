import {Injectable} from '@angular/core';
import {DriverRaceResult, League, Race, RaceResultsService, RaceService} from '../../../../build/openapi';
import {BehaviorSubject} from 'rxjs';
import {LeagueViewService} from '../league-view/services/league-view.service';
import {TokenService} from '../../shared/services/token.service';
import {UiService} from '../../shared/services/ui.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RaceEditService {

  race: Race = undefined;
  raceChanged: BehaviorSubject<Race> = new BehaviorSubject<Race>(this.race);

  raceResultsToPost: DriverRaceResult[] = [];

  constructor(private raceService: RaceService,
              private raceResultsService: RaceResultsService,
              private leagueViewService: LeagueViewService,
              private tokenService: TokenService,
              private uiService: UiService,
              private router: Router) {
  }

  loadRaceToEdit(leagueId: string, raceId: string): void {
    this.raceService.getRaces(leagueId, raceId).subscribe(races => {
      this.race = races[0];
      this.raceChanged.next(this.race);
    });
  }

  addRaceToRaceResults(raceResultToAdd: DriverRaceResult): void {
    if (this.raceResultsToPost.map(raceResult => raceResult.driverId).includes(raceResultToAdd.driverId)) {
      // todo remove old input
    }

    this.raceResultsToPost.push(raceResultToAdd);
    this.raceResultsToPost.sort((a, b) => a.endpostion - b.endpostion);
  }

  postRaceResults(): void {
    this.raceResultsService.postRaceResultsToLeagueByLeagueIdAndRaceId(this.leagueViewService.selectedLeague.leagueId, this.race.raceId, this.tokenService.getCurrentToken(), this.raceResultsToPost).subscribe(raceResults => {
      this.uiService.showSnackBar('Added ' + raceResults.length + ' to race ' + (this.race.racePositionIndex + 1) + ' ' + this.race.circuit, 5000);
      this.navigateToLeagueViewFor(this.leagueViewService.selectedLeague);
    }, error => {
      this.uiService.showSnackBar('Error while trying to save raceresults: ' + error, 5000);
    });
  }

  navigateToLeagueViewFor(league: League): void {
    this.router.navigate(['league'], {queryParams: {leagueId: league.leagueId}});
  }

  // todo clear driver results list if user navigates back!
}
