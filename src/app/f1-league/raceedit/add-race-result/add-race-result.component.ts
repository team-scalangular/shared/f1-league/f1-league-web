import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Car, Driver, DriverRaceResult, Race, RaceResultsService} from '../../../../../build/openapi';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RaceEditService} from '../race-edit.service';
import {LeagueViewService} from '../../league-view/services/league-view.service';
import {UiService} from '../../../shared/services/ui.service';

@Component({
  selector: 'app-add-race-result',
  templateUrl: './add-race-result.component.html',
  styleUrls: ['./add-race-result.component.scss']
})
export class AddRaceResultComponent implements OnInit {
  @Output() addedRaceResult: EventEmitter<void> = new EventEmitter<void>();
  @Input() driver: Driver;
  addRaceResultFormGroup: FormGroup;
  race: Race;

  constructor(private raceResultsService: RaceResultsService,
              private raceEditService: RaceEditService,
              public leagueViewService: LeagueViewService,
              private uiService: UiService) {
  }

  ngOnInit(): void {
    this.raceEditService.raceChanged.subscribe(race => this.race = race);
    this.initializeFormGroups();
  }

  public getCars() {
    return Object.values(Car);
  }

  public addRaceResultToList(driver: Driver) {
    if (this.addRaceResultFormGroup.invalid) {
      this.uiService.showSnackBar('Invalid values. Please check your input.', 5000);
      return;
    }

    const raceResultToPost: DriverRaceResult = {
      raceId: this.race.raceId,
      fastestLap: this.addRaceResultFormGroup.get('fastestLap').value,
      startposition: this.addRaceResultFormGroup.get('grid').value,
      endpostion: this.addRaceResultFormGroup.get('endPosition').value,
      carInRace: this.addRaceResultFormGroup.get('car').value,
      driverId: driver.driverId,
      driverName: driver.name + ' ' + driver.surename
    };

    this.raceEditService.addRaceToRaceResults(raceResultToPost);
    this.addedRaceResult.emit();
  }

  private initializeFormGroups() {
    this.addRaceResultFormGroup = new FormGroup({
      grid: new FormControl('', [Validators.required]),
      endPosition: new FormControl('', [Validators.required]),
      fastestLap: new FormControl(''),
      car: new FormControl('', [Validators.required])
    });
  }
}
