import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LeagueCreationService} from './services/league-creation.service';
import {MatStepper} from '@angular/material/stepper';
import {Driver, Team} from '../../../../build/openapi';
import {Router} from '@angular/router';

@Component({
  selector: 'app-league-creation',
  templateUrl: './league-creation.component.html',
  styleUrls: ['./league-creation.component.scss']
})
export class LeagueCreationComponent implements OnInit, OnDestroy {
  createLeagueForm: FormGroup;
  createTeamsForm: FormGroup;
  createDriverForm: FormGroup;
  leagueHasTeams = false;
  createdTeamsForLeague: Team[] = [];
  hasCreatedLeague = false;

  constructor(
    private leagueCreationService: LeagueCreationService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.initializeFormGroups();
  }

  createLeague(stepper: MatStepper) {
    if (this.createLeagueForm.get('name').valid) {
      this.leagueCreationService.createLeagueFromName(this.createLeagueForm.get('name').value);
      this.hasCreatedLeague = true;
      this.createLeagueForm.get('hasCreatedLeague').setValue(true);
      stepper.next();
    } else {
      window.alert('invalid league name');
    }
  }

  createTeamForLeague() {
    if (this.createTeamsForm.valid) {
      this.leagueCreationService.createTeamForLeague(this.createTeamsForm.get('name').value);
    } else {
      window.alert('invalid team name');
    }
  }

  onLeagueHasTeamsChanged(event: Team[]) {
    this.createdTeamsForLeague = event;
    this.leagueHasTeams = !!event;
  }

  addDriverToLeague() {
    if (this.createDriverForm.valid) {
      const teamToAddDriverTo: Team = this.createDriverForm.get('team').value;
      const driverToAdd: Driver = {
        name: this.createDriverForm.get('name').value,
        surename: this.createDriverForm.get('surename').value,
        teamId: teamToAddDriverTo.teamId
      };
      this.leagueCreationService.addDriverToLeague(driverToAdd);
    } else {
      window.alert('invalid values');
    }
  }

  navigateToLandingPage() {
    this.router.navigate(['']);
  }

  ngOnDestroy(): void {
    sessionStorage.removeItem('editorToken');
  }

  private initializeFormGroups() {
    this.createLeagueForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      hasCreatedLeague: new FormControl(false, [Validators.requiredTrue])
    });
    this.createTeamsForm = new FormGroup({
      name: new FormControl('', [Validators.required])
    });
    this.createDriverForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      surename: new FormControl('', [Validators.required]),
      team: new FormControl(null, [Validators.required])
    });
  }
}
