import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Driver, League} from '../../../../../build/openapi';
import {LeagueCreationService} from '../services/league-creation.service';
import {Subscription} from 'rxjs';

export interface DriverStandingRow {
  position: number;
  driver: string;
  team?: string;
  points: number;
}

@Component({
  selector: 'app-drivers-table',
  templateUrl: './drivers-table.component.html',
  styleUrls: ['./drivers-table.component.scss']
})
export class DriversTableComponent implements OnInit, OnDestroy {
  createdLeague: League;
  drivers: Driver[] = [];
  columnsToDisplay = ['position', 'driver', 'points'];
  dataSource: MatTableDataSource<DriverStandingRow> = new MatTableDataSource<DriverStandingRow>();
  noContent = false;
  subscriptions: Subscription = new Subscription();

  constructor(
    private leagueCreationService: LeagueCreationService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.leagueCreationService.createdLeagueChanged.subscribe(league => {
        this.createdLeague = league;
        this.subscriptions.add(this.leagueCreationService.driversForCreatedLeagueChanged
          .subscribe(drivers => {
            this.drivers = drivers;
            if (this.drivers.length !== 0) {
              this.loadDriversToTable();
            }
          })
        );
      })
    );
    this.dataSource.connect().asObservable().subscribe(value => this.noContent = this.dataSource.data.length === 0);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  private loadDriversToTable(): void {
    this.clearDataSource();
    this.drivers.forEach(driver => {
      const aDriverRow: DriverStandingRow = {
        position: driver.position,
        driver: driver.name + ' ' + driver.surename,
        points: driver.points
      };

      const data = this.dataSource.data;
      data.push(aDriverRow);
      data.sort((a, b) => a.position - b.position);
      this.dataSource.data = data;
    });
  }

  private clearDataSource() {
    let data: DriverStandingRow[];
    data = [];
    this.dataSource.data = data;
  }
}
