import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {League, Team} from '../../../../../build/openapi';
import {MatTableDataSource} from '@angular/material/table';
import {LeagueCreationService} from '../services/league-creation.service';
import {Subscription} from 'rxjs';

export interface TeamStandingRow {
  position: number;
  team: string;
  points: number;
}

@Component({
  selector: 'app-teams-table',
  templateUrl: './teams-table.component.html',
  styleUrls: ['./teams-table.component.scss']
})
export class TeamsTableComponent implements OnInit, OnDestroy {
  createdLeague: League;
  columnsToDisplay = ['position', 'team', 'points'];
  teams: Team[];
  dataSource: MatTableDataSource<TeamStandingRow> = new MatTableDataSource<TeamStandingRow>();
  noContent = false;
  @Output() leagueHasTeamsChanged: EventEmitter<Team[]> = new EventEmitter<Team[]>();
  subscriptions: Subscription = new Subscription();

  constructor(
    private leagueCreationService: LeagueCreationService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.leagueCreationService.createdLeagueChanged.subscribe(league => {
        this.createdLeague = league;
        this.subscriptions.add(this.leagueCreationService.teamsForCreatedLeagueChanged
          .subscribe(teams => {
            this.teams = teams;
            this.leagueHasTeamsChanged.emit(this.teams);
            if (teams.length !== 0) {
              this.loadTeamsToTable();
            }
          })
        );
      })
    );
    this.subscriptions
      .add(this.dataSource.connect().asObservable().subscribe(value => this.noContent = this.dataSource.data.length === 0));
  }

  loadTeamsToTable(): void {
    this.clearDataSource();
    this.teams.forEach(team => {
      const aTeamRow: TeamStandingRow = {
        position: team.position,
        team: team.name,
        points: team.points
      };

      const data = this.dataSource.data;
      data.push(aTeamRow);
      data.sort((a, b) => a.position - b.position);
      this.dataSource.data = data;
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  private clearDataSource() {
    let data: TeamStandingRow[];
    data = [];
    this.dataSource.data = data;
  }
}
