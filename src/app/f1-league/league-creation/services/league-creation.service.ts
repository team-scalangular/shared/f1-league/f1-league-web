import {Injectable} from '@angular/core';
import {Driver, DriverService, League, LeagueService, Team, TeamService} from '../../../../../build/openapi';
import {Subject} from 'rxjs';
import {UiService} from '../../../shared/services/ui.service';

@Injectable({
  providedIn: 'root'
})
export class LeagueCreationService {
  createdLeague: League = undefined;
  createdLeagueChanged: Subject<League> = new Subject<League>();

  teamsForCreatedLeague: Team[] = [];
  teamsForCreatedLeagueChanged: Subject<Team[]> = new Subject<Team[]>();

  driversForCreatedLeague: Driver[] = [];
  driversForCreatedLeagueChanged: Subject<Driver[]> = new Subject<Driver[]>();

  constructor(
    private leagueService: LeagueService,
    private teamService: TeamService,
    private driverService: DriverService,
    private uiService: UiService
  ) {
  }

  createLeagueFromName(leagueName: string): void {
    const leagueToCreate: League = {
      name: leagueName
    };
    this.leagueService.postLeague(leagueToCreate, 'response')
      .subscribe(response => {
        this.createdLeague = response.body;
        this.createdLeagueChanged.next(this.createdLeague);
        const editorToken = response.headers.get('X-LeagueEditorToken');
        sessionStorage.setItem('editorToken', editorToken);
        this.uiService.openDialogFor(editorToken);
      });
  }

  createTeamForLeague(teamName: string): void {
    this.teamService.postTeamToLeague(this.createdLeague.leagueId, sessionStorage.getItem('editorToken'), {name: teamName})
      .subscribe((createdTeam) => {
        this.updateTeamsFromLeague();
      });
  }

  addDriverToLeague(driverToAdd: Driver) {
    this.driverService.postDriverToTeamByLeagueIdAndTeamId(
      this.createdLeague.leagueId,
      driverToAdd.teamId,
      sessionStorage.getItem('editorToken'),
      driverToAdd)
      .subscribe(createdDriver => {
        this.updateTeamsFromLeague();
      });
  }

  private updateTeamsFromLeague() {
    this.teamService.getTeamsByLeagueId(this.createdLeague.leagueId)
      .subscribe(teams => {
        this.teamsForCreatedLeague = teams;
        this.teamsForCreatedLeagueChanged.next(this.teamsForCreatedLeague);
        this.updateDriverFromLeague();
      });
  }

  private updateDriverFromLeague() {
    this.driversForCreatedLeague = [];
    let receivedTeams = 0;
    let deltaForEmptyTeams = 0;
    this.teamsForCreatedLeague.forEach(team => {
      if (team.listOfDriverIds.length !== 2) {
        deltaForEmptyTeams += (2 - team.listOfDriverIds.length);
      }
      team.listOfDriverIds.forEach(driverId => {
        this.driverService.getDriverById(driverId)
          .subscribe(driver => {
            this.driversForCreatedLeague.push(driver);
            receivedTeams++;
            if (receivedTeams === this.teamsForCreatedLeague.length * 2 - deltaForEmptyTeams) {
              this.emitChanges();
            }
          });
      });
    });
  }

  private emitChanges() {
    this.driversForCreatedLeagueChanged.next(this.driversForCreatedLeague);
  }
}
