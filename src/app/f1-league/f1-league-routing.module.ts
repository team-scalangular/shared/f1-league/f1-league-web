import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {LeagueViewComponent} from './league-view/league-view.component';
import {ClassificationsComponent} from './classifications/classifications.component';
import {RacesComponent} from './races/races.component';
import {TeamsComponent} from './teams/teams.component';
import {DriverComponent} from './driver/driver.component';
import {LeagueCreationComponent} from './league-creation/league-creation.component';
import {TeamEditComponent} from './teams/team-edit/team-edit.component';
import {RaceeditComponent} from './raceedit/raceedit.component';
import {AddRaceComponent} from './add-race/add-race.component';

const routes: Routes = [
  {path: '', component: LandingPageComponent},
  {path: 'league', component: LeagueViewComponent},
  {path: 'league/classifications', component: ClassificationsComponent},
  {path: 'league/races', component: RacesComponent},
  {path: 'league/teams', component: TeamsComponent},
  {path: 'league/team-edit', component: TeamEditComponent},
  {path: 'league/driver', component: DriverComponent},
  {path: 'leagueCreation', component: LeagueCreationComponent},
  {path: 'league/edit/raceResults', component: RaceeditComponent},
  {path: 'add/race', component: AddRaceComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class F1LeagueRoutingModule {
}
