import {Component, OnInit} from '@angular/core';
import {Circuit, Race, RaceService} from '../../../../build/openapi';
import {LeagueViewService} from '../league-view/services/league-view.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UiService} from '../../shared/services/ui.service';
import {TokenService} from '../../shared/services/token.service';

@Component({
  selector: 'app-add-race',
  templateUrl: './add-race.component.html',
  styleUrls: ['./add-race.component.scss']
})
export class AddRaceComponent implements OnInit {

  createRaceForm: FormGroup;
  isLoading = false;

  constructor(
    public leagueViewService: LeagueViewService,
    private raceService: RaceService,
    private uiService: UiService,
    private tokenService: TokenService
  ) {
  }

  ngOnInit(): void {
    this.initializeFormGroups();
  }

  getAllCircuits() {
    return Object.keys(Circuit).map(key => Circuit[key]).filter(value => typeof value === 'string') as string[];
  }

  addRaceToLeague() {
    if (this.createRaceForm.valid) {
      const raceToPost: Race = {
        circuit: this.createRaceForm.get('circuit').value,
        hasBeenDriven: this.createRaceForm.get('hasBeenDriven').value,
        racePositionIndex: this.createRaceForm.get('racePositionIndex').value - 1,
        raceDate: this.createRaceForm.get('raceDate').value
      };

      this.isLoading = true;

      this.raceService
        .postRaceToLeagueByLeagueId(
          this.leagueViewService.selectedLeagueChanges.getValue().leagueId,
          this.tokenService.getCurrentToken(),
          raceToPost)
        .subscribe(race => {
          this.uiService.showSnackBar('Added race ' + (race.racePositionIndex + 1) + ' in ' + race.circuit, 5000);
        }, error => {
          this.uiService.showSnackBar('error while trying to create the race ' + error, 5000);
          this.isLoading = false;
        }, () => {
          this.isLoading = false;
          this.initializeFormGroups();
        });
    } else {
      this.uiService.showSnackBar('Invalid values. Please check your input.', 5000);
    }
  }

  private initializeFormGroups() {
    this.createRaceForm = new FormGroup({
      circuit: new FormControl('', [Validators.required]),
      racePositionIndex: new FormControl('', [Validators.required]),
      hasBeenDriven: new FormControl(false),
      raceDate: new FormControl('', Validators.required)
    });
  }
}
