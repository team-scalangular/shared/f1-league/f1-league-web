import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {filter, flatMap} from 'rxjs/operators';
import {Driver, DriverService, League, Team, TeamService} from '../../../../build/openapi';
import {LeagueViewService} from './services/league-view.service';
import {Subscription} from 'rxjs';
import {TokenService} from '../../shared/services/token.service';

@Component({
  selector: 'app-league-view',
  templateUrl: './league-view.component.html',
  styleUrls: ['./league-view.component.scss']
})
export class LeagueViewComponent implements OnInit, OnDestroy {
  loaded = false;
  isEditor: boolean;

  leagueId: string;
  selectedLeague: League;
  allTeamsForCurrentLeague: Team[] = [];
  allDriversForCurrentLeague: Driver[] = [];
  mapDriverToTeam: Map<string, Team> = new Map<string, Team>();
  mapTeamToDrivers: Map<string, Driver[]> = new Map<string, Driver[]>();

  subscriptions: Subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private teamService: TeamService,
    private driverService: DriverService,
    private leagueViewService: LeagueViewService,
    private tokenService: TokenService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.tokenService.tokenIsSet.subscribe(isSet => {
        this.isEditor = isSet;
      })
    );
    this.subscriptions.add(this.leagueViewService.allDriversFromCurrentLeagueChanged.subscribe(drivers => {
        this.allDriversForCurrentLeague = drivers;
        this.allDriversForCurrentLeague.sort((a, b) => a.position - b.position);
      })
    );
    this.subscriptions.add(this.leagueViewService.allTeamsFromCurrentLeagueChanged.subscribe(teams => {
        this.allTeamsForCurrentLeague = teams;
        this.allTeamsForCurrentLeague.sort((a, b) => a.position - b.position);
        this.leagueViewService.setAllDriversFromCurrentLeague(teams);
      })
    );
    this.subscriptions.add(this.route.queryParams
      .pipe(
        filter(params => params.leagueId),
        flatMap(params => {
          this.leagueId = params.leagueId;
          this.leagueViewService.setSelectedLeague(this.leagueId);
          return this.leagueViewService.selectedLeagueChanges;
        })
      )
      .pipe(filter(leagueId => leagueId !== undefined))
      .subscribe(selectedLeague => {
        this.selectedLeague = selectedLeague;
        this.leagueViewService.setAllTeamsFromCurrentLeague(selectedLeague.leagueId);
      })
    );
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  navigateToAddRace() {
    this.router.navigate(['add/race']);
  }
}
