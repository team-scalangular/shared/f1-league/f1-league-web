import {Injectable} from '@angular/core';
import {Driver, DriverService, League, LeagueService, Team, TeamService} from '../../../../../build/openapi';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LeagueViewService {
  selectedLeague: League = undefined;
  selectedLeagueChanges: BehaviorSubject<League> = new BehaviorSubject<League>(this.selectedLeague);

  allTeamsFromCurrentLeague: Team[] = [];
  allTeamsFromCurrentLeagueChanged: BehaviorSubject<Team[]> = new BehaviorSubject<Team[]>(this.allTeamsFromCurrentLeague);

  allDriversFromCurrentLeague: Driver[] = [];
  allDriversFromCurrentLeagueChanged: BehaviorSubject<Driver[]> = new BehaviorSubject<Driver[]>(this.allDriversFromCurrentLeague);
  loadingDriversFinished = false;

  mapDriverToTeam: Map<string, Team> = new Map<string, Team>();
  mapDriverToTeamChanged: BehaviorSubject<Map<string, Team>> = new BehaviorSubject<Map<string, Team>>(this.mapDriverToTeam);

  mapTeamToDrivers: Map<string, Driver[]> = new Map<string, Driver[]>();
  mapTeamToDriversChanged: BehaviorSubject<Map<string, Driver[]>> = new BehaviorSubject<Map<string, Driver[]>>(this.mapTeamToDrivers);

  constructor(
    private leagueService: LeagueService,
    private driverService: DriverService,
    private teamService: TeamService
  ) {
  }

  setAllTeamsFromCurrentLeague(leagueId: string): void {
    this.teamService.getTeamsByLeagueId(leagueId).subscribe(teams => {
      this.allTeamsFromCurrentLeague = teams;
      this.allTeamsFromCurrentLeagueChanged.next(teams);
    });
  }

  setSelectedLeague(leagueId): void {
    this.leagueService.getLeagues(leagueId).subscribe(league => {
      this.selectedLeague = league[0];
      this.selectedLeagueChanges.next(this.selectedLeague);
    });
  }

  setAllDriversFromCurrentLeague(teams: Team[]): void {
    let receivedTeams = 0;
    let deltaForEmptyTeams = 0;

    if (teams == null) {
      this.loadingDriversFinished = true;
      this.emitChanges();
      return;
    }

    teams.forEach(team => {
      if (team.listOfDriverIds.length !== 2) {
        deltaForEmptyTeams += (2 - team.listOfDriverIds.length);
      }
      team.listOfDriverIds?.forEach((driverId, index) => {
        this.driverService.getDriverById(driverId).subscribe(driver => {
          this.allDriversFromCurrentLeague.push(driver);
          this.mapDriverToTeam.set(driver.driverId, team);
          if (this.mapTeamToDrivers.get(team.teamId)) {
            this.mapTeamToDrivers.get(team.teamId).push(driver);
          } else {
            this.mapTeamToDrivers.set(team.teamId, [driver]);
          }
          receivedTeams++;
          if (receivedTeams === teams.length * 2 - deltaForEmptyTeams) {
            this.loadingDriversFinished = true;
            this.emitChanges();
          }
        });
      });
    });
  }

  emitChanges(): void {
    this.allDriversFromCurrentLeagueChanged.next(this.allDriversFromCurrentLeague);
    this.mapDriverToTeamChanged.next(this.mapDriverToTeam);
    this.mapTeamToDriversChanged.next(this.mapTeamToDrivers);
  }

  clearAll(): void {
    this.selectedLeague = undefined;
    this.allDriversFromCurrentLeague = [];
    this.allTeamsFromCurrentLeague = [];
    this.mapTeamToDrivers.clear();
    this.mapDriverToTeam.clear();
    this.selectedLeagueChanges.next(this.selectedLeague);
    this.allTeamsFromCurrentLeagueChanged.next(this.allTeamsFromCurrentLeague);
    this.loadingDriversFinished = false;
    this.emitChanges();
  }
}
