import {Injectable} from '@angular/core';
import {Driver, DriverService, Team, TeamService} from '../../../../../build/openapi';
import {Subject} from 'rxjs';
import {LeagueViewService} from '../../league-view/services/league-view.service';
import {Router} from '@angular/router';
import {TokenService} from '../../../shared/services/token.service';

@Injectable({
  providedIn: 'root'
})
export class TeamEditService {
  currentLeagueId: string;
  currentLeagueIdChanged: Subject<string> = new Subject<string>();

  teamToUpdate: Team;
  teamToUpdateChanged: Subject<Team> = new Subject<Team>();

  drivers: Driver[] = [];

  constructor(
    private leagueViewService: LeagueViewService,
    private teamService: TeamService,
    private router: Router,
    private tokenService: TokenService,
    private driverService: DriverService
  ) {
  }

  setCurrentLeagueId(leagueId: string): void {
    this.currentLeagueId = leagueId;
    this.currentLeagueIdChanged.next(this.currentLeagueId);
  }

  fetchTeamToUpdate(teamId: string): void {
    this.teamToUpdate = this.leagueViewService.allTeamsFromCurrentLeague.find(team => team.teamId === teamId);
    this.drivers = this.leagueViewService.mapTeamToDrivers.get(this.teamToUpdate.teamId);
    this.teamToUpdateChanged.next(this.teamToUpdate);
  }

  clearAll(): void {
    this.currentLeagueId = undefined;
    this.drivers = [];
    this.teamToUpdate = undefined;
  }

  updateTeam(valuesToUpdate: Partial<Team>): void {
    const leagueEditorToken = this.tokenService.getCurrentToken();
    if (leagueEditorToken === null) {
      window.alert('No token set!');
      this.router.navigate(['']);
      return;
    }
    const teamToUpdate = {
      ...this.teamToUpdate,
      name: valuesToUpdate.name
    };
    this.teamService.patchTeam(leagueEditorToken, teamToUpdate, this.currentLeagueId)
      .subscribe(updatedTeam => {
        this.router.navigate(['league'], {queryParams: {leagueId: this.currentLeagueId}});
      }, error => console.log(error.message));
  }

  addDriverToTeam(driverToAdd: Driver): void {
    this.driverService.patchDriver(this.tokenService.getCurrentToken(), driverToAdd, this.teamToUpdate.teamId, this.currentLeagueId)
      .subscribe(driver => {
        console.log('updatet driver');
        console.log(driver);
      });
  }
}
