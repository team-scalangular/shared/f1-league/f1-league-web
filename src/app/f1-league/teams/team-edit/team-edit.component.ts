import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {filter} from 'rxjs/operators';
import {Driver, Team} from '../../../../../build/openapi';
import {TeamEditService} from '../services/team-edit.service';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LeagueViewService} from '../../league-view/services/league-view.service';

@Component({
  selector: 'app-team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.scss']
})
export class TeamEditComponent implements OnInit, OnDestroy {
  selectedLeagueId: string;
  teamToUpdate: Team;
  drivers: Driver[] = [];

  leagueLoaded = false;
  teamLoaded = false;

  subscriptions: Subscription = new Subscription();
  teamUpdateForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private teamEditService: TeamEditService,
    private leagueViewService: LeagueViewService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.teamEditService.currentLeagueIdChanged
        .subscribe(leagueId => {
          this.selectedLeagueId = leagueId;
          this.leagueLoaded = true;
        })
    );
    this.subscriptions.add(
      this.teamEditService.teamToUpdateChanged
        .subscribe(team => {
          this.teamToUpdate = team;
          this.drivers = this.teamEditService.drivers;
          this.initializeFormGroup();
          this.teamLoaded = true;
        })
    );
    this.route.queryParams
      .pipe(filter(params => params.leagueId || params.teamId))
      .subscribe(params => {
        this.teamEditService.setCurrentLeagueId(params.leagueId);
        this.teamEditService.fetchTeamToUpdate(params.teamId);
      });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
    this.teamEditService.clearAll();
    this.leagueViewService.clearAll();
  }

  onSaveTeam() {
    console.log(this.teamUpdateForm);
    if (this.teamUpdateForm.valid) {
      this.teamEditService.updateTeam({name: this.teamUpdateForm.get('name').value});
    }
    if (this.teamUpdateForm.get('driverToAdd').value !== null) {
      this.teamEditService.addDriverToTeam(this.teamUpdateForm.get('driverToAdd').value);
    }
  }

  getAllDriversFromCurrentLeague(): Driver[] {
    return this.filteredDrivers(this.leagueViewService.allDriversFromCurrentLeague);
  }

  private initializeFormGroup() {
    this.teamUpdateForm = new FormGroup({
      name: new FormControl(this.teamToUpdate.name, [Validators.required]),
      driverToAdd: new FormControl(null)
    });
  }

  private filteredDrivers(allDriver: Driver[]): Driver[] {
    const driverIdsInTeam: string[] = [];
    if (!this.drivers) {
      return allDriver;
    } else {
      this.drivers.forEach(driverInTeam => {
        driverIdsInTeam.push(driverInTeam.driverId);
      });
      return allDriver.filter(driver => {
        return !driverIdsInTeam.includes(driver.driverId);
      });
    }
  }
}
