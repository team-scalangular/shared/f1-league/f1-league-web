import {Component, Input, OnInit} from '@angular/core';
import {Driver, DriverService, Team} from '../../../../build/openapi';
import {LeagueViewService} from '../league-view/services/league-view.service';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import {Router} from '@angular/router';
import {TokenService} from '../../shared/services/token.service';

@Component({
  selector: 'app-team',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  loaded = false;
  editorTokenIsSet: boolean;

  @Input() teamToShow: Team;
  drivers: Driver[] = [];
  mapTeamToDrivers: Map<string, Driver[]> = new Map<string, Driver[]>();

  subscriptions: Subscription = new Subscription();

  constructor(
    private driverService: DriverService,
    private leagueViewService: LeagueViewService,
    private router: Router,
    private tokenService: TokenService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.tokenService.tokenIsSet.subscribe(isSet => {
        this.editorTokenIsSet = isSet;
      })
    );
    this.subscriptions.add(this.leagueViewService.mapTeamToDriversChanged
      .pipe(filter(responseMap => responseMap.size !== 0))
      .subscribe(responseMap => {
        this.mapTeamToDrivers = responseMap;
        this.loaded = true;
      })
    );
  }

  printDriverForTeams(): string {
    let driversText = '';
    this.mapTeamToDrivers.get(this.teamToShow.teamId)?.forEach((driver: Driver, index: number) => {
      driversText = driversText + driver.name + ' ' + driver.surename + ', ';
      if (index === this.mapTeamToDrivers.get(this.teamToShow.teamId).length - 1) {
        driversText = driversText.substring(0, driversText.length - 2);
      }
    });
    return driversText;
  }

  redirectToTeamEditFromTeam(): void {
    this.router.navigate(['league', 'team-edit'], {
      queryParamsHandling: 'merge',
      queryParams: {teamId: this.teamToShow.teamId}
    });
  }
}
