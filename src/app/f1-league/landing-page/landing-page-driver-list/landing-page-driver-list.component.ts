import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {DriverService, League} from '../../../../../build/openapi';
import {LandingPageService} from '../service/landing-page.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';

export interface DriverWithTeam {
  name: string;
  points: number;
  teamName: string;
}

@Component({
  selector: 'app-landing-page-driver-list',
  templateUrl: './landing-page-driver-list.component.html',
  styleUrls: ['./landing-page-driver-list.component.scss']
})
export class LandingPageDriverListComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() league: League;
  displayedColumns: string[] = ['name', 'team', 'points'];
  dataSource: MatTableDataSource<DriverWithTeam> = new MatTableDataSource<DriverWithTeam>();
  @Output() loadingDriverFinished = new EventEmitter<void>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  loaded = false;
  subscriptions: Subscription = new Subscription();
  noContent = false;

  constructor(
    private landingPageService: LandingPageService,
    private driverService: DriverService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.landingPageService.fetchTeamsFromLeague(this.league)
      .pipe(filter(teams => {
        if (teams == null) {
          this.loaded = true;
          this.noContent = this.dataSource.data?.length === 0 || !this.dataSource.data;
        }
        return teams !== null;
      }))
      .subscribe(teams => {
        teams.forEach(team => {
          team.listOfDriverIds.forEach(driverId => {
            this.subscriptions.add(this.driverService.getDriverById(driverId).subscribe(driver => {
                const data = this.dataSource.data;
                data.push({name: driver.name, points: driver.points, teamName: team.name});
                this.dataSource.data = data;
              }, error => {
              }, () => {
                this.loaded = true;
                this.noContent = this.dataSource.data?.length === 0 || !this.dataSource.data;
              })
            );
          });
        });
      })
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
