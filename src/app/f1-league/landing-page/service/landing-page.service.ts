import {Injectable} from '@angular/core';
import {League, LeagueService, Team, TeamService} from '../../../../../build/openapi';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LandingPageService {
  availableLeagues: League[] = [];
  availableLeaguesChanged: Subject<League[]> = new Subject<League[]>();

  constructor(
    private leagueService: LeagueService,
    private teamService: TeamService,
    private router: Router
  ) {
  }

  fetchTeamsFromLeague(league: League): Observable<Team[]> {
    return this.teamService.getTeamsByLeagueId(league.leagueId);
  }

  getAllAvailableLeagues(): void {
    this.leagueService.getLeagues().subscribe(leagues => {
      this.availableLeagues = leagues;
      this.availableLeaguesChanged.next(this.availableLeagues);
    });
  }

  navigateToLeagueViewFor(league: League): void {
    this.router.navigate(['league'], {queryParams: {leagueId: league.leagueId}});
  }
}
