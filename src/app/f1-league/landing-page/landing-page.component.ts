import {Component, OnInit} from '@angular/core';
import {LandingPageService} from './service/landing-page.service';
import {League} from '../../../../build/openapi';
import {Router} from '@angular/router';
import {LeagueViewService} from '../league-view/services/league-view.service';
import {TokenService} from '../../shared/services/token.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  leagues: League[];
  loading = false;

  constructor(
    private landingPageService: LandingPageService,
    private router: Router,
    private leagueViewService: LeagueViewService,
    private tokenService: TokenService
  ) {
  }

  forwardToLeagueView(league: League): void {
    this.landingPageService.navigateToLeagueViewFor(league);
  }

  ngOnInit(): void {
    this.tokenService.clearToken();
    this.leagueViewService.clearAll();
    this.landingPageService.availableLeaguesChanged.subscribe(leagues => {
      this.leagues = leagues;
      this.loading = false;
    });
    this.landingPageService.getAllAvailableLeagues();
  }

  navigateToLeagueCreation() {
    this.router.navigate(['leagueCreation']);
  }
}
