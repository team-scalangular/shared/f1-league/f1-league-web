import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {League, Team} from '../../../../../build/openapi';
import {LandingPageService} from '../service/landing-page.service';
import {Subscription} from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-landing-page-team-list',
  templateUrl: './landing-page-team-list.component.html',
  styleUrls: ['./landing-page-team-list.component.scss']
})
export class LandingPageTeamListComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() league: League;
  teamsFromCurrentLeagueSub: Subscription;
  displayedColumns: string[] = ['name', 'points'];
  dataSource: MatTableDataSource<Team> = new MatTableDataSource<Team>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  loaded = false;
  noContent = false;
  subscriptions: Subscription = new Subscription();

  constructor(private landingPageService: LandingPageService) {
  }

  ngOnInit(): void {
    this.teamsFromCurrentLeagueSub = this.landingPageService.fetchTeamsFromLeague(this.league)
      .subscribe((teams) => {
        this.dataSource.data = teams;
        this.loaded = true;
        this.noContent = this.dataSource.data?.length === 0 || !this.dataSource.data;
      });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    if (this.teamsFromCurrentLeagueSub) {
      this.teamsFromCurrentLeagueSub.unsubscribe();
    }
    this.subscriptions.unsubscribe();
  }
}
