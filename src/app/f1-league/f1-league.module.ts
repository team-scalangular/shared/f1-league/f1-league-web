import {NgModule} from '@angular/core';
import {MaterialModule} from '../material.module';
import {CommonModule} from '@angular/common';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {LeagueViewComponent} from './league-view/league-view.component';
import {ClassificationsComponent} from './classifications/classifications.component';
import {RacesComponent} from './races/races.component';
import {TeamsComponent} from './teams/teams.component';
import {DriverComponent} from './driver/driver.component';
import {F1LeagueRoutingModule} from './f1-league-routing.module';

import {MatRippleModule} from '@angular/material/core';

import {LandingPageTeamListComponent} from './landing-page/landing-page-team-list/landing-page-team-list.component';
import {LandingPageDriverListComponent} from './landing-page/landing-page-driver-list/landing-page-driver-list.component';
import {DriverStandingComponent} from './classifications/driver-standing/driver-standing.component';
import {TeamStandingComponent} from './classifications/team-standing/team-standing.component';
import {LeagueCreationComponent} from './league-creation/league-creation.component';
import {MatStepperModule} from '@angular/material/stepper';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {RaceViewComponent} from './races/race-view/race-view.component';
import {TimePipePipe} from '../shared/time-pipe.pipe';
import {TeamsTableComponent} from './league-creation/teams-table/teams-table.component';
import {DriversTableComponent} from './league-creation/drivers-table/drivers-table.component';
import {TeamEditComponent} from './teams/team-edit/team-edit.component';
import {RaceeditComponent} from './raceedit/raceedit.component';
import {AddRaceResultComponent} from './raceedit/add-race-result/add-race-result.component';
import {AddRaceComponent} from './add-race/add-race.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
  declarations: [
    LandingPageComponent,
    LeagueViewComponent,
    ClassificationsComponent,
    RacesComponent,
    TeamsComponent,
    DriverComponent,
    LandingPageTeamListComponent,
    LandingPageDriverListComponent,
    DriverStandingComponent,
    TeamStandingComponent,
    LeagueCreationComponent,
    RaceViewComponent,
    TimePipePipe,
    TeamsTableComponent,
    TeamsTableComponent,
    DriversTableComponent,
    TeamEditComponent,
    RaceeditComponent,
    AddRaceResultComponent,
    AddRaceComponent
  ],
  exports: [
    LandingPageComponent,
    DriverComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    F1LeagueRoutingModule,
    MatRippleModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatDatepickerModule
  ]
})
export class F1LeagueModule {
}
