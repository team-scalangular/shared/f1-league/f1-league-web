import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Driver, League, Team, TeamClassificationService} from '../../../../../build/openapi';
import {LeagueViewService} from '../../league-view/services/league-view.service';
import {Subscription} from 'rxjs';
import {filter, flatMap} from 'rxjs/operators';

export interface TeamStandingRow {
  position: number;
  team: string;
  drivers: string;
  points: number;
}

@Component({
  selector: 'app-team-standing',
  templateUrl: './team-standing.component.html',
  styleUrls: ['./team-standing.component.scss']
})
export class TeamStandingComponent implements OnInit, OnDestroy {
  loaded = false;

  teams: Team[] = [];
  mapTeamToDrivers: Map<string, Driver[]> = new Map<string, Driver[]>();
  selectedLeague: League;

  columnsToDisplay = ['position', 'team', 'drivers', 'points'];
  dataSource: MatTableDataSource<TeamStandingRow> = new MatTableDataSource<TeamStandingRow>();

  subscription: Subscription = new Subscription();
  noContent = false;

  constructor(
    private leagueViewService: LeagueViewService,
    private teamClassificationService: TeamClassificationService
  ) {
  }

  ngOnInit(): void {
    this.subscription.add(
      this.leagueViewService.selectedLeagueChanges
        .pipe(
          filter(league => league !== undefined),
          flatMap(league => {
            this.selectedLeague = league;
            return this.teamClassificationService.getTeamClassificationByLeagueId(this.selectedLeague.leagueId);
          }),
          flatMap(teams => {
            this.teams = teams;
            return this.leagueViewService.mapTeamToDriversChanged;
          })
        ).subscribe(responseMap => {
        this.mapTeamToDrivers = responseMap;
        this.loadTeamsToTable();
      })
    );
    this.subscription
      .add(this.dataSource.connect().asObservable().subscribe(value => {
          this.noContent = this.dataSource.data?.length === 0 || !this.dataSource.data;
        }
      ));
  }

  loadTeamsToTable(): void {
    this.clearDataSource();
    this.teams.forEach(team => {
      let driverString = '';
      this.mapTeamToDrivers.get(team.teamId)?.forEach(driver => {
        driverString = driverString + ' ' + driver.name;
      });

      const aTeamRow: TeamStandingRow = {
        position: team.position,
        team: team.name,
        drivers: driverString,
        points: team.points
      };

      const data = this.dataSource.data;
      data.push(aTeamRow);
      data.sort((a, b) => a.position - b.position);
      this.dataSource.data = data;
    });
    this.loaded = true;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private clearDataSource() {
    let data: TeamStandingRow[];
    data = [];
    this.dataSource.data = data;
  }
}
