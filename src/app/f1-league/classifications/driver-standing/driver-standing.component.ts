import {Component, OnDestroy, OnInit} from '@angular/core';
import {Driver, Team} from '../../../../../build/openapi';
import {MatTableDataSource} from '@angular/material/table';
import {LeagueViewService} from '../../league-view/services/league-view.service';
import {flatMap} from 'rxjs/operators';
import {Subscription} from 'rxjs';

export interface DriverStandingRow {
  position: number;
  driver: string;
  team: string;
  points: number;
}

@Component({
  selector: 'app-driver-standing',
  templateUrl: './driver-standing.component.html',
  styleUrls: ['./driver-standing.component.scss']
})
export class DriverStandingComponent implements OnInit, OnDestroy {
  loaded = false;
  noContent = false;

  drivers: Driver[] = [];
  mapDriverToTeam: Map<string, Team> = new Map<string, Team>();

  columnsToDisplay = ['position', 'driver', 'team', 'points'];
  dataSource: MatTableDataSource<DriverStandingRow> = new MatTableDataSource<DriverStandingRow>();

  subscription: Subscription = new Subscription();

  constructor(
    private leagueViewService: LeagueViewService,
  ) {
  }

  ngOnInit(): void {
    this.subscription.add(this.leagueViewService.allDriversFromCurrentLeagueChanged
      .pipe(
        flatMap(drivers => {
          this.drivers = drivers;
          return this.leagueViewService.mapDriverToTeamChanged;
        })
      ).subscribe(responseMap => {
        this.mapDriverToTeam = responseMap;
        this.loaded = this.leagueViewService.loadingDriversFinished;
        this.loadDriversToTable();
      })
    );
  }

  loadDriversToTable(): void {
    this.clearDataSource();
    this.drivers.forEach(driver => {
      const teamFromDriver: string = this.mapDriverToTeam.get(driver.driverId).name;
      const aDriverRow: DriverStandingRow = {
        position: driver.position,
        driver: driver.name + ' ' + driver.surename,
        team: teamFromDriver,
        points: driver.points
      };

      const data = this.dataSource.data;
      data.push(aDriverRow);
      data.sort((a, b) => a.position - b.position);
      this.dataSource.data = data;
    });

    this.noContent = this.dataSource.data?.length === 0 || !this.dataSource.data;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private clearDataSource() {
    let data: DriverStandingRow[];
    data = [];
    this.dataSource.data = data;
  }
}
