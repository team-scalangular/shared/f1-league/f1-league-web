import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {ApiModule} from '../../build/openapi';
import {HttpClientModule} from '@angular/common/http';
import {SidenavComponent} from './header/sidenav/sidenav.component';
import {ToolbarComponent} from './header/toolbar/toolbar.component';
import {SharedModule} from './shared/shared.module';
import {F1LeagueModule} from './f1-league/f1-league.module';
import {StoreModule} from '@ngrx/store';
import {reducers} from './app.reducer';
import {EffectsModule} from '@ngrx/effects';
import {LeaguesEffects} from './shared/store/leagues/leagues.effects';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ApiModule,
    HttpClientModule,
    SharedModule,
    F1LeagueModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([LeaguesEffects]),
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
