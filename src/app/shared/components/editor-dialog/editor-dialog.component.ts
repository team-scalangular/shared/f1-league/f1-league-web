import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

class DialogData {
}

@Component({
  selector: 'app-editor-dialog',
  templateUrl: './editor-dialog.component.html',
  styleUrls: ['./editor-dialog.component.scss']
})
export class EditorDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditorDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { token: string }) {
  }

  ngOnInit(): void {
  }

}
