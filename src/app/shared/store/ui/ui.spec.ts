import * as fromUi from './ui.reducer';
import * as fromUiActions from './ui.actions';

describe('Ui reducer tests', () => {

  describe('Undefined action', () => {
    it('should return the default state for no action', () => {
      const mockedInitialState = fromUi.initialState;
      const action = {type: undefined};
      const state = fromUi.uiReducer(mockedInitialState, action);

      expect(state).toBe(mockedInitialState);
    });
  });

  describe('START_LOADING Action', () => {
    it('should set isLoading to true', () => {
      const mockedInitialState = fromUi.initialState;
      const action = new fromUiActions.StartLoading();
      const state = fromUi.uiReducer(mockedInitialState, action);

      expect(state.isLoading).toEqual(true);
      expect(state.isDarkThemed).toEqual(mockedInitialState.isDarkThemed);
    });
  });

  describe('STOP_LOADING Action', () => {
    it('should set isLoading to false', () => {
      const mockedInitialState = fromUi.initialState;
      const action = new fromUiActions.StopLoading();
      const state = fromUi.uiReducer(mockedInitialState, action);

      expect(state.isLoading).toEqual(false);
      expect(state.isDarkThemed).toEqual(mockedInitialState.isDarkThemed);
    });
  });

  describe('SWITCH_COLOR_THEME Action', () => {
    it('should switch isDarkThemed from true to false', () => {
      const mockedInitialState = fromUi.initialState;
      const action = new fromUiActions.SwitchColorTheme();
      const state = fromUi.uiReducer(mockedInitialState, action);

      expect(state.isDarkThemed).toEqual(!mockedInitialState.isDarkThemed);
      expect(state.isLoading).toEqual(false);
    });

    it('should switch isDarkThemed from false to true', () => {
      const previousState = {...(fromUi.initialState), isDarkThemed: false};
      const action = new fromUiActions.SwitchColorTheme();
      const state = fromUi.uiReducer(previousState, action);

      expect(state.isDarkThemed).toEqual(true);
      expect(state.isLoading).toEqual(previousState.isLoading);
    });
  });
});
