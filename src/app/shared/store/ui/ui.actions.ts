import {Action} from '@ngrx/store';

export const START_LOADING = '[UI] Start Loading';
export const STOP_LOADING = '[UI] Stop Loading';

export const SWITCH_COLOR_THEME = '[UI] Switch Color Theme';

export class StartLoading implements Action {
  readonly type = START_LOADING;
}

export class StopLoading implements Action {
  readonly type = STOP_LOADING;
}

export class SwitchColorTheme implements Action {
  readonly type = SWITCH_COLOR_THEME;
}

export type uiActions = StartLoading | StopLoading | SwitchColorTheme;
