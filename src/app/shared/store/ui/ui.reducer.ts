import {START_LOADING, STOP_LOADING, SWITCH_COLOR_THEME, uiActions} from './ui.actions';

export interface State {
  isLoading: boolean;
  isDarkThemed: boolean;
}

export const initialState: State = {
  isLoading: false,
  isDarkThemed: true
};

export function uiReducer(state = initialState, action: uiActions) {
  switch (action.type) {
    case START_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case STOP_LOADING:
      return {
        ...state,
        isLoading: false
      };
    case SWITCH_COLOR_THEME:
      return {
        ...state,
        isDarkThemed: !state.isDarkThemed
      };
    default:
      return state;
  }
}

export const getIsLoading = (state: State) => state.isLoading;
export const getIsDarkThemed = (state: State) => state.isDarkThemed;
