import * as fromLeagues from './leagues.reducer';
import * as fromLeaguesAction from './leagues.action';
import {LoadingState} from '../interfaces/call-state';
import {League} from '../../../../../build/openapi';

describe('Leagues Reducer Tests', () => {
  let initialState: fromLeagues.State;
  const mockLeague: League = {leagueId: '123', name: 'mockLeague', listOfTeamIds: ['123'], listOfRaceIds: ['123']};
  const mockLeaguesResponse: League[] = [mockLeague, mockLeague];

  beforeEach(() => {
    initialState = fromLeagues.initialState;
  });

  describe('Undefined action', () => {
    it('should return the initial state', () => {
      const mockAction = {type: undefined};
      const returnedState = fromLeagues.leaguesReducer(initialState, mockAction);

      expect(returnedState).toEqual(initialState);
    });
  });

  describe('START_LOADING action', () => {
    it('should set the callState to loading', () => {
      const mockAction = new fromLeaguesAction.StartLoading();
      const returnedState = fromLeagues.leaguesReducer(initialState, mockAction);

      expect(returnedState.callState).toEqual(LoadingState.LOADING);
    });
  });

  describe('LOADING_SUCCESS action', () => {
    it('should set callState to Loaded and save the payload', () => {
      const mockAction = new fromLeaguesAction.LoadingSuccess(mockLeaguesResponse);
      const returnedState = fromLeagues.leaguesReducer(initialState, mockAction);

      expect(returnedState.callState).toEqual(LoadingState.LOADED);
      expect(returnedState.leagues).toEqual(mockLeaguesResponse);
    });
  });

  describe('LOADING_FAILED action', () => {
    it('should set callState to an http error message', () => {
      const mockAction = new fromLeaguesAction.LoadingFailed(new Error('an error').message);
      const returnedState = fromLeagues.leaguesReducer(initialState, mockAction);

      // @ts-ignore
      expect(returnedState.callState.errorMessage).toEqual('an error');
    });
  });

  describe('SELECT_LEAGUE action', () => {
    it('should set the selected League to the dispatched league', () => {
      const mockAction = new fromLeaguesAction.SelectLeague(mockLeague);
      const returnedState = fromLeagues.leaguesReducer(initialState, mockAction);

      expect(returnedState.selectedLeague).toEqual(mockLeague);
    });
  });

  describe('REMOVE_LEAGUE action', () => {
    it('should set the state to the initial state', () => {
      const mockAction = new fromLeaguesAction.RemoveLeagues();
      const returnedState = fromLeagues.leaguesReducer(initialState, mockAction);

      expect(returnedState).toEqual(initialState);
    });
  });
});
