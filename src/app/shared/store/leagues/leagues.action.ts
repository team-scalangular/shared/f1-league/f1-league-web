import {Action} from '@ngrx/store';
import {League} from '../../../../../build/openapi';

export const START_LOADING = '[Leagues] Start Loading';
export const LOADING_SUCCESS = '[Leagues] Loading Success';
export const LOADING_FAILED = '[Leagues] Loading Failed';
export const SELECT_LEAGUE = '[Leagues] Select League';
export const REMOVE_LEAGUES = '[Leagues] Remove Leagues';

export class StartLoading implements Action {
  readonly type = START_LOADING;
}

export class LoadingSuccess implements Action {
  readonly type = LOADING_SUCCESS;

  constructor(public payload: League[]) {
  }
}

export class LoadingFailed implements Action {
  readonly type = LOADING_FAILED;

  constructor(public payload: any) {
  }
}

export class SelectLeague implements Action {
  readonly type = SELECT_LEAGUE;

  constructor(public payload: League) {
  }
}

export class RemoveLeagues implements Action {
  readonly type = REMOVE_LEAGUES;
}

export type leaguesActions = StartLoading | LoadingSuccess | LoadingFailed | SelectLeague | RemoveLeagues;
