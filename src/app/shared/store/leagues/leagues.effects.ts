import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import * as leaguesActions from './leagues.action';
import {catchError, map, switchMap} from 'rxjs/operators';
import {League, LeagueService} from '../../../../../build/openapi';

@Injectable()
export class LeaguesEffects {

  @Effect()
  loadLeagues$ = this.actions.pipe(
    ofType(leaguesActions.START_LOADING))
    .pipe(
      switchMap(() => {
        return this.leagueService.getLeagues().pipe(
          map((leagues: League[]) => new leaguesActions.LoadingSuccess(leagues)),
          catchError(err => of(new leaguesActions.LoadingFailed(err.message)))
        );
      })
    );

  constructor(
    private actions: Actions,
    private leagueService: LeagueService
  ) {
  }
}
