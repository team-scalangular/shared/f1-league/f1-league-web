import {CallState, LoadingState} from '../interfaces/call-state';
import {
  leaguesActions,
  LOADING_FAILED,
  LOADING_SUCCESS,
  REMOVE_LEAGUES,
  SELECT_LEAGUE,
  START_LOADING
} from './leagues.action';
import {League} from '../../../../../build/openapi';

export interface State {
  leagues: League[];
  selectedLeague: League;
  callState: CallState;
}

export const initialState: State = {
  leagues: [],
  selectedLeague: undefined,
  callState: LoadingState.INIT
};

export function leaguesReducer(state = initialState, action: leaguesActions): State {
  switch (action.type) {
    case START_LOADING:
      return {
        ...state,
        callState: LoadingState.LOADING
      };
    case LOADING_SUCCESS:
      return {
        ...state,
        leagues: action.payload,
        callState: LoadingState.LOADED
      };
    case LOADING_FAILED:
      return {
        ...state,
        callState: {errorMessage: action.payload}
      };
    case SELECT_LEAGUE:
      return {
        ...state,
        selectedLeague: action.payload
      };
    case REMOVE_LEAGUES:
      return initialState;
    default:
      return state;
  }
}

export const getLeagues = (state: State) => state.leagues;
export const getSelectedLeague = (state: State) => state.selectedLeague;
