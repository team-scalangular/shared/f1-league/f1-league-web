import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export const EDITOR_TOKEN = 'editorToken';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  tokenIsSet: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  setToken(tokenToSet: string): void {
    sessionStorage.setItem(EDITOR_TOKEN, tokenToSet);
    this.tokenIsSet.next(true);
  }

  clearToken(): void {
    sessionStorage.removeItem(EDITOR_TOKEN);
    this.tokenIsSet.next(false);
  }

  getCurrentToken(): string | null {
    if (sessionStorage.getItem(EDITOR_TOKEN)) {
      return sessionStorage.getItem(EDITOR_TOKEN);
    } else {
      return null;
    }
  }
}
