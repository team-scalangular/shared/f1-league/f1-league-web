import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {EditorDialogComponent} from '../components/editor-dialog/editor-dialog.component';
import {TokenDialogComponent} from '../components/token-dialog/token-dialog.component';
import {TokenService} from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  constructor(
    private snackBar: MatSnackBar,
    private matDialog: MatDialog,
    private tokenService: TokenService
  ) {
  }

  showSnackBar(message: string, duration: number, action?: string): void {
    this.snackBar.open(message, action, {
      duration
    });
  }

  openDialogFor(leagueEditorToken: string): void {
    this.matDialog.open(EditorDialogComponent, {
      data: {token: leagueEditorToken}
    });
  }

  openTokenDialog(): void {
    const dialogRef = this.matDialog.open(TokenDialogComponent, {
      data: {editorToken: ''}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === '' || result === undefined) {
        window.alert('Your token was empty or you need to click ok to save it!');
        return;
      }
      this.tokenService.setToken(result);
    });
  }
}
