import {NgModule} from '@angular/core';
import {EditorDialogComponent} from './components/editor-dialog/editor-dialog.component';
import {MaterialModule} from '../material.module';
import {TokenDialogComponent} from './components/token-dialog/token-dialog.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [EditorDialogComponent, TokenDialogComponent],
  imports: [MaterialModule, FormsModule]
})
export class SharedModule {
}
