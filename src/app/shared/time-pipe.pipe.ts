import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'timePipe'
})
export class TimePipePipe implements PipeTransform {

  transform(time: number): string {
    const minutes: number = Math.floor(time / 60);
    return minutes + ':' + (time - minutes * 60).toFixed(3);
  }

}
