import * as fromRoot from './app.reducer';
import * as fromUi from './shared/store/ui/ui.reducer';

describe('App state selectors', () => {

  describe('Ui isLoading selectors', () => {

    it('should return false for the initial state', () => {
      expect(fromRoot.getIsLoading.projector(fromUi.initialState)).toBe(false);
    });

    it('should return true if the app is loading', () => {
      const modifiedState: fromUi.State = {
        ...fromUi.initialState,
        isLoading: true
      };
      expect(fromRoot.getIsLoading.projector(modifiedState)).toBe(true);
    });
  });

  describe('Ui theme selectors', () => {

    it('should return true for the initial state', () => {
      expect(fromRoot.getIsDarkThemed.projector(fromUi.initialState)).toBe(true);
    });

    it('should return false if isDarkThemed is false', () => {
      const modifiedState: fromUi.State = {
        ...fromUi.initialState,
        isDarkThemed: false
      };
      expect(fromRoot.getIsDarkThemed.projector(modifiedState)).toBe(false);
    });
  });
});
